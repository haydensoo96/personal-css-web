const userService = require('../services/users')



class UserController {
    async createUser(req, res) {
        try {
            const id = await userService.createUser(req.body);
            var data = id.values();
            var obj = {};
            obj.id = data.next().value;
            const result = await userService.getUserInfo(obj)
            return res.render('login.ejs', { LoginError: 'Register Successful' })
        } catch (err) {
            console.error(err);
            res.status(500).json('Something went wrong')
        }
    }

    async getImages(res) {
        try {
            const images = await userService.getImagesInfo();

            return images
        } catch (err) {
            console.error(err)
            res.status(500).json('Get Images Error')
        }
    }

    async uploadImages(req, res) {
        console.log('Body in Controller : ', req)
        try {
            const images = await userService.uploadImages(req)
            return true
        } catch (err) {
            console.error(err);
            res.status(500).json('Something went wrong')
        }
    }

    async deleteImages(req, res) {
        console.log('Body in Controller : ', req.id)
        try {
            const images = await userService.deleteImages(req.id)
            return true
        } catch (err) {
            console.error(err);
            res.status(500).json('Something went wrong')
        }
    }

    async editImageInfo(req, res) {
        console.log('Body in Controller : ', req)
        try {
            const images = await userService.editImages(req)
            return true
        } catch (err) {
            console.error(err);
            res.status(500).json('Something went wrong')
        }
    }

    async getEventList(res) {
        try {
            const events = await userService.getEvents()
            return events
        } catch (err) {
            console.error(err);
            res.status(500).json('Something went wrong')
        }
    }

    async addEvent(req, res) {
        console.log('Body in Controller : ', req)
        try {
            const event = await userService.addEvent(req)
            return true
        } catch (err) {
            console.error(err);
            res.status(500).json('Something went wrong')
        }
    }
}

module.exports = new UserController();