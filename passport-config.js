var LocalStrategy = require('passport-local').Strategy
var userService = require('./services/users')
var bcrypt = require('bcrypt')
var passport = require('passport')




module.exports = function (passport, sessionstorage) {

    passport.serializeUser(function (user, done) {
        var obj = {};
        obj.email = user.email
        userService.getUserInfo(obj).then(result => {
            done(null, result.id);
        });
    })

    passport.deserializeUser(function (id, done) {
        var obj = {};
        obj.id = id;
        userService.getUserInfo(obj).then(result => {
            if (result) {
                return done(null, result);
            } else {
                var msg = 'User Not Found'
                done(msg, null)
            }
        });
    })

    passport.use("local-signin", new LocalStrategy({ usernameField: 'email', passwordField: "password", passReqToCallback: true },
        function (req, username, password, done) {
            var obj = {};
            obj.email = username;
            obj.password = password;
            var isValidPassword = async function (userpass, password) {
               return await bcrypt.compare(password, userpass)
            };

            userService.getUserInfo(obj).then(result => {
                var user = result;
                console.log(result)

                if (!result){
                    console.log('Username undefined')
                    return done(null, false, {
                        LoginError: "Invalid Username"
                    })
                }
                
                isValidPassword(user.password,obj.password).then(result =>{
                    if(result===false){
                        console.log('Wrong Password')
                        return done(null,false,{LoginError:'Wrong Password ...'})
                    }else{
                        return done(null,user)
                    }
                })
            }).catch(
                function (err) {
                    console.log("Error:", err);
                    return done(null, false);
                });
        }

    ))
}

