// Update with your config settings.
// const knex = require('knex')({
//   client: 'mysql',
//   connection: {
//     host: '127.0.0.1',
//     port: 3306,
//     user: 'root',
//     password: 'devguy',
//     database: 'haydenweb'
//   },
//   migrations:{
//     tableName:'knex_migrations'
//   }
// });

module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: '127.0.0.1',
      port: 3306,
      user: 'root',
      password: 'devguy',
      database: 'haydenweb'
    },
    migrations: {
      // tableName: 'knex_migrations',
      directory: ('./db/migrations'),
    },
  }
}



