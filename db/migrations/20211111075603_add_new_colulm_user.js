
exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('users', table => {
        table.string('imageUrl')
    })
};

exports.down = async function(knex, Promise) {
    await knex.schema.alterTable('users', table => {
        table.dropColumn('imageUrl');
    })
};
