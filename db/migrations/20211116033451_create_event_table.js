exports.up = async function (knex, Promise) {
    await knex.schema.createTable('events', function (t) {
        t.increments('id').primary().unique().notNullable()
        t.string('title')
        t.date('date')
        t.string('createdBy')
    })
};


exports.down = async function (knex, Promise) {
    await knex.schema.dropTableIfExists('events')
}

