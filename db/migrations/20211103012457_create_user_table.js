exports.up = async function (knex, Promise) {
    await knex.schema.createTable('users', function (t) {
        t.increments('id').primary().unique().notNullable()
        t.string('name')
        t.string('email').notNullable().unique()
        t.string('password');
        t.integer('role')
    })
};


exports.down = async function (knex, Promise) {
    await knex.schema.dropTableIfExists('users')
}

