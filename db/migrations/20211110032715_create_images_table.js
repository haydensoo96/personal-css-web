exports.up = async function (knex, Promise) {
    await knex.schema.createTable('images', function (t) {
        t.increments('id').primary().unique().notNullable()
        t.string('title')
        t.string('imageUrl')
        t.string('description');
    })
};


exports.down = async function (knex, Promise) {
    await knex.schema.dropTableIfExists('images')
}

