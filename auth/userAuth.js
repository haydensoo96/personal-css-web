const userService = require('../services/users')

exports.isAuthenticatedUser = function(req,res,next){
    console.log('Authenticated:',req.isAuthenticated())
    if(req.isAuthenticated()){
            return next();
    }else{
        res.redirect('/login')
    }
}