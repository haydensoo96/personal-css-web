const express = require('express');
const router = express.Router();
const userController = require('../controller/users');
const passport = require('passport')
const auth = require("../auth/userAuth")
const moment = require('moment')


router.get('/addImages', auth.isAuthenticatedUser, (req, res) => {
    res.render('addImages.ejs')
})

router.get('/editImages', auth.isAuthenticatedUser, (req, res) => {
    userController.getImages().then(result => {
        console.log('Image List:', result)
        res.render('editImages.ejs', { images: result, user: req.user })
    })
})

router.get('/eventlist', auth.isAuthenticatedUser, (req, res) => {
    userController.getEventList().then(result => {

        console.log('Event Result :', result)
        if(!result[0]){
            var dummy = {title:'None', date:'None', newDate:'None', createBy:'None'}
            
            result.push(dummy)
            console.log('Emtpy detected:',result)
        }
        for (var i of result) {
            console.log('iDate:', i.date)
            let newDate = moment(i.date).format('DD-MM-YYYY')
            console.log('New Date:', newDate)
            i.date = newDate
            console.log('List:', i)
        }
        res.json(result)
    })
})

router.get('/event', auth.isAuthenticatedUser, (req, res) => {
    userController.getEventList().then(result => {
        if(!result[0]){
            var dummy = {title:'None', date:'20-20-2020', newDate:'20-20-2020', createBy:'None'}
            
            result.push(dummy)
            console.log('Emtpy detected:',result)
        }
        for (var i of result) {
            console.log('iDate:', i.date)
            let newDate = moment(i.date).format('DD-MM-YYYY')
            console.log('New Date:', newDate)
            i.newDate = newDate
            console.log('List:', i)
        }
        res.render('event.ejs', { event: result })
    })
})






module.exports = router;