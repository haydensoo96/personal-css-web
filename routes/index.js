const express = require('express');
const router = express.Router();
const userController = require('../controller/users');
const passport = require('passport')
const auth = require("../auth/userAuth")
const moment = require('moment');
const { data } = require('jquery');

var now = moment(new Date()); //todays date
var end = moment("2019-01-29"); // another date
var duration = moment.duration(now.diff(end));
var date = {};
date.days = Math.floor(duration.asDays(String))
date.date = end
console.log('Date:', JSON.stringify(date))




function getDays() {
    var now = moment(new Date()); //todays date
    var end = moment("2019-01-29"); // another date
    var duration = moment.duration(now.diff(end));
    var date = {};
    date.days = Math.floor(duration.asDays(String))
    date.date = end
    console.log('Date:', JSON.stringify(date))
    return date
}



///HOME PAGE ///
router.get('/', auth.isAuthenticatedUser, (req, res) => {
    let date = getDays();
    userController.getImages().then(result => {
        res.render('main.ejs', { role: 'Admin', images: result, user: req.user, date: date })
    })
})

router.get('/home', auth.isAuthenticatedUser, (req, res) => {
    let date = getDays();
    console.log('UserInfo:', req.user)
    userController.getImages().then(result => {
        res.render('main.ejs', { role: 'Admin', images: result, user: req.user, date: date })
    })
})
///HOME PAGE ///


/// IMAGE ///
router.post('/uploadImages', auth.isAuthenticatedUser, (req, res) => {
    var data = req.body;
    console.log('Image Data:', data)
    userController.uploadImages(data).then(result => {
        console.log('API Result:', result)
        if (result === true) {
            return res.render('addImages.ejs', { message: 'Successful Added Post...' })
        } else {
            return res.render('addImages.ejs', { message: 'Fail Adding Post... ' })
        }
    })
})

router.get('/deleteImages/(:id)', auth.isAuthenticatedUser, (req, res) => {
    var data = req.params;
    userController.deleteImages(data).then(result => {
        if (result === true) {
            userController.getImages().then(result => {
                res.render('editImages.ejs', { images: result, user: req.user, message: 'Images Deleted' })
            })
        } else {
            userController.getImages().then(result => {
                res.render('editImages.ejs', { images: result, user: req.user, message: 'Images Delete Fail' })
            })
        }
    })
})

router.post('/editImagesInfo', auth.isAuthenticatedUser, (req, res) => {
    var data = req.body;
    userController.editImageInfo(data).then(result => {
        if (result === true) {
            userController.getImages().then(result => {
                res.render('editImages.ejs', { images: result, user: req.user, message: 'Images Info Updated' })
            })
        } else {
            userController.getImages().then(result => {
                res.render('editImages.ejs', { images: result, user: req.user, message: 'Images Info Update Fail' })
            })
        }
    })
})

/// IMAGE ///



///EVENT///

router.post('/addEvent', auth.isAuthenticatedUser, (req, res) => {
    var data = {};
    data.title = req.body.title;
    data.date = req.body.date;
    data.username = req.user.name
    console.log('API Data:', data)
    userController.addEvent(data).then(result => {
        if (result === true) {
            res.redirect('user/event')
        } else {
            res.json('Something Went Wrong')
        }
    })
})

///EVENT///



///LOGIN LOGOUT///


router.get('/login', (req, res) => {
    res.render('login.ejs')
})

router.get('/register', (req, res) => {
    res.render('register.ejs')
})

router.post('/register', userController.createUser);


router.post('/login', (req, res, next) => {
    passport.authenticate('local-signin', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.render('login.ejs', { LoginError: info.LoginError })
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }
            return next()
        });
    })(req, res, next);
}, (req, res, next) => {
    console.log('Body:', req.body)
    console.log('Login User: ', req.user)
    if (req.user.role === 1) {
        res.redirect('/home')
    }
})



router.get('/logout', (req, res, next) => {
    if (req.user) {
        req.session.destroy(function (err) {
            res.redirect('/login')
        })
    } else {
        res.redirect('/');
    }
})


///LOGIN LOGOUT///

module.exports = router;