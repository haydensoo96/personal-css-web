if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
};

var express = require('express')
var app = express()
var initializePassport = require('./passport-config');
var passport = require('passport');
var flash = require('express-flash');
var session = require('express-session');



initializePassport(passport)
var MemoryStore = session.MemoryStore;
var sessionstorage = new MemoryStore();


app.use(flash())
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store:sessionstorage
}))

app.use(passport.initialize());
app.use(passport.session());

app.use(express.urlencoded({ extended: false }));
app.set('view-engine', 'ejs');



var userRoute = require('./routes/users');
var indexRoute = require('./routes/index')

app.use('/user', userRoute);
app.use(express.static(__dirname + '/'), indexRoute);




app.listen(3000)