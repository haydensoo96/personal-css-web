const UsersDAO = require('../dao/users')
class UsersService {
    createUser(body) {
        const { name, email, password } = body;
        return UsersDAO.createUser(name, email, password)
    }

    getUserInfo(body){
        return UsersDAO.getUserInfo(body)
    }

    getImagesInfo(){
        return UsersDAO.getImagesInfo()
    }

    uploadImages(body){
        return UsersDAO.uploadImages(body)
    }

    deleteImages(data){
        return UsersDAO.deleteImages(data)
    }

    editImages(data){
        return UsersDAO.editImages(data)
    }

    getEvents(){
        return UsersDAO.getEvents()
    }

    addEvent(data){
        return UsersDAO.addEvent(data)
    }
}

module.exports = new UsersService();