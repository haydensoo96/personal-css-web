const db = require('../db/db')
const bcrypt = require('bcrypt')


class UsersDAO {
    async createUser(name, email, password, role) {
        const hashedPassword = await bcrypt.hash(password, 10)
        const query = await db('users').insert({
            name: name,
            email: email,
            password: hashedPassword,
            role: 1
        })
        return query
    }

    async getUserInfo(data) {
        if (data.id) {
            const query = await db('users').select('*').where({ id: data.id })
            return query[0]
        } else if (data.email) {
            const query = await db('users').select('*').where({ email: data.email })

            return query[0]
        }
    }

    async getImagesInfo() {
        const query = await db('images').select('*').whereNot('imageUrl', "")
        return query;
    }

    async uploadImages(data) {
        const query = await db('images')
            .insert({ title: data.title, imageUrl: data.imageUrl, description: data.description })
        return query;
    }

    async deleteImages(data) {
        const query = await db('images')
            .del()
            .where({ id: data })
        return query;
    }

    async editImages(data) {
        const query = await db('images')
            .update({ title: data.title, description: data.description })
            .where({ id: data.id })
        return query;
    }

    async getEvents() {
        const query = await db('events').select('*').orderBy('date', 'asc').where('date','>',new Date())
        return query;
    }

    async addEvent(data) {
        const query = await db('events').insert({ title: data.title, date: data.date, createdBy: data.username })
        return query;
    }
}

module.exports = new UsersDAO();